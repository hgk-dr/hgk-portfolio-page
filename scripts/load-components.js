// loadComponents.js
function loadComponent(componentId, filePath) {
    fetch(filePath).then(function(response) {
        return response.text();
    }).then(function(html) {
        document.getElementById(componentId).innerHTML = html;
    }).catch(function(err) {
        console.warn('Something went wrong.', err);
    });
}

// Load the header and footer components
document.addEventListener('DOMContentLoaded', (event) => {
    loadComponent("header", "includes/header.html");
    loadComponent("footer", "includes/footer.html");
});